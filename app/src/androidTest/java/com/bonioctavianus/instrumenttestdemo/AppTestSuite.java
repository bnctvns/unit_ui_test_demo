package com.bonioctavianus.instrumenttestdemo;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by bonioctavianus on 9/12/16.
 */

@RunWith(Suite.class)
@Suite.SuiteClasses({ButtonActivityTest.class, EditActivityTest.class})
public class AppTestSuite {


}
