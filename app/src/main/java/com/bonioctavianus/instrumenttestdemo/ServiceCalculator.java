package com.bonioctavianus.instrumenttestdemo;

/**
 * Created by bonioctavianus on 9/12/16.
 */
public interface ServiceCalculator {

    int add(int num1, int num2);

    int substract(int num1, int num2);

    int multiply(int num1, int num3);

    int divide(int num1, int num2);
}
